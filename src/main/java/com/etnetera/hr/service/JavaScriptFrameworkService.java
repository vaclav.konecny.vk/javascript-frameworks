package com.etnetera.hr.service;

import com.etnetera.hr.data.entity.JavaScriptFrameworkEntity;
import com.etnetera.hr.data.entity.JavaScriptFrameworkVersionEntity;
import com.etnetera.hr.data.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.data.repository.JavaScriptFrameworkVersionRepository;
import com.etnetera.hr.rest.DtoMapper;
import com.etnetera.hr.rest.dto.JavaScriptFrameworkDTO;
import com.etnetera.hr.rest.dto.JavaScriptFrameworkVersionDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Vaclav Konecny
 */
@Service
public class JavaScriptFrameworkService {

    JavaScriptFrameworkVersionRepository versionRepository;
    JavaScriptFrameworkRepository frameworkRepository;

    public JavaScriptFrameworkService(JavaScriptFrameworkVersionRepository versionRepository, JavaScriptFrameworkRepository frameworkRepository) {
        this.versionRepository = versionRepository;
        this.frameworkRepository = frameworkRepository;
    }

    public List<JavaScriptFrameworkDTO> findAllFrameworks() {
        Iterable<JavaScriptFrameworkEntity> all = frameworkRepository.findAll();
        return StreamSupport.stream(all.spliterator(), false)
                .map(DtoMapper::toDto)
                .collect(Collectors.toList());

    }

    public JavaScriptFrameworkDTO findFrameworkById(Long id) {
        JavaScriptFrameworkEntity javaScriptFrameworkEntity = frameworkRepository.findById(id).orElseThrow();
        return DtoMapper.toDto(javaScriptFrameworkEntity);
    }

    public JavaScriptFrameworkDTO saveFramework(JavaScriptFrameworkDTO dto) {
        JavaScriptFrameworkEntity newEntity = new JavaScriptFrameworkEntity();
        newEntity.setName(dto.getName());
        JavaScriptFrameworkEntity saved = frameworkRepository.save(newEntity);
        return DtoMapper.toDto(saved);
    }

    public JavaScriptFrameworkDTO updateFramework(JavaScriptFrameworkDTO framework) {
        JavaScriptFrameworkEntity entity = frameworkRepository.findById(framework.getId()).orElseThrow();
        entity.setName(framework.getName());
        return DtoMapper.toDto(frameworkRepository.save(entity));
    }

    public void deleteFramework(Long id) {
        if (frameworkRepository.existsById(id)) {
            frameworkRepository.deleteById(id);
        } else {
            throw new NoSuchElementException();
        }
    }

    public List<JavaScriptFrameworkVersionDTO> getVersions(Long frameworkId) {
        List<JavaScriptFrameworkVersionEntity> versions = frameworkRepository.findById(frameworkId).orElseThrow().getVersions();
        return versions.stream().map(DtoMapper::toDto).collect(Collectors.toList());
    }

    public JavaScriptFrameworkVersionDTO getVersion(Long versionId) {
        JavaScriptFrameworkVersionEntity version = versionRepository.findById(versionId).orElseThrow();
        return DtoMapper.toDto(version);
    }

    public JavaScriptFrameworkVersionDTO saveVersion(JavaScriptFrameworkVersionDTO versionDTO, Long frameworkId) {
        JavaScriptFrameworkEntity frameworkEntity = frameworkRepository.findById(frameworkId).orElseThrow();
        JavaScriptFrameworkVersionEntity newEntity = new JavaScriptFrameworkVersionEntity();
        newEntity.setFramework(frameworkEntity);
        newEntity.setVersion(versionDTO.getVersion());
        newEntity.setDeprecate(versionDTO.getDeprecate());
        newEntity.setHype(versionDTO.getHype());

        return DtoMapper.toDto(versionRepository.save(newEntity));
    }

    public JavaScriptFrameworkVersionDTO updateVersion(Long versionId, JavaScriptFrameworkVersionDTO updateVersion) {
        JavaScriptFrameworkVersionEntity byId = versionRepository.findById(versionId).orElseThrow();
        byId.setVersion(updateVersion.getVersion());
        byId.setDeprecate(updateVersion.getDeprecate());
        byId.setHype(updateVersion.getHype());
        return DtoMapper.toDto(versionRepository.save(byId));
    }

    public void deleteVersion(Long frameworkId, Long versionId) {
        JavaScriptFrameworkVersionEntity frameworkVersion = versionRepository.findById(versionId).orElseThrow();
        if (!Objects.equals(frameworkVersion.getFramework().getId(), frameworkId)) {
            throw new NoSuchElementException("FrameworkId doesn't belong to Version");
        }
        versionRepository.deleteById(versionId);
    }

    public List<JavaScriptFrameworkDTO> findFrameworkByName(String search) {
        List<JavaScriptFrameworkEntity> allByNameContainingIgnoreCase = frameworkRepository.findAllByNameContainingIgnoreCase(search);
        return allByNameContainingIgnoreCase.stream()
                .map(DtoMapper::toDto)
                .collect(Collectors.toList());
    }
}
