package com.etnetera.hr.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;

/**
 * @author Vaclav Konecny
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@With
public class JavaScriptFrameworkVersionDTO {
    private long id;
    @PositiveOrZero(message = "Hype must by positive or zero number.")
    private Integer hype;
    @NotBlank(message = "Version must not be blank.")
    private String version;
    @NotNull(message = "Deprecate date must be filled.")
    private LocalDate deprecate;
    private long frameworkId;
}
