package com.etnetera.hr.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author Vaclav Konecny
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JavaScriptFrameworkDTO {
    private long id;
    @NotBlank
    private String name;
    private List<Long> versionsId;
}
