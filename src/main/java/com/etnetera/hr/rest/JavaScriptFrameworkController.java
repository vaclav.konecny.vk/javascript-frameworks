package com.etnetera.hr.rest;

import com.etnetera.hr.rest.dto.JavaScriptFrameworkDTO;
import com.etnetera.hr.rest.dto.JavaScriptFrameworkVersionDTO;
import com.etnetera.hr.service.JavaScriptFrameworkService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Simple REST controller for accessing application logic.
 *
 * @author Etnetera
 */
@RestController
public class JavaScriptFrameworkController {

    private final JavaScriptFrameworkService service;

    public JavaScriptFrameworkController(JavaScriptFrameworkService service) {
        this.service = service;
    }

    @GetMapping("/frameworks")
    public List<JavaScriptFrameworkDTO> frameworks(@RequestParam(required = false, value = "search") String search) {
        if (search == null) {
            return service.findAllFrameworks();
        } else {
            return service.findFrameworkByName(search);
        }
    }

    @GetMapping("/frameworks/{id}")
    public JavaScriptFrameworkDTO frameworkById(@PathVariable Long id) {
        return service.findFrameworkById(id);
    }

    @PostMapping("/frameworks")
    @ResponseStatus(code = HttpStatus.CREATED)
    public JavaScriptFrameworkDTO saveFramework(@Valid @RequestBody JavaScriptFrameworkDTO framework) {
        return service.saveFramework(framework);
    }

    @PutMapping("/frameworks/{id}")
    public JavaScriptFrameworkDTO updateFramework(@Valid @RequestBody JavaScriptFrameworkDTO framework, @PathVariable Long id) {
        return service.updateFramework(framework);
    }

    @DeleteMapping("/frameworks/{id}")
    public void deleteFramework(@PathVariable Long id) {
        service.deleteFramework(id);
    }

    // todo VK pokud bude požadavek doplnit search s criteria api
    @GetMapping("/frameworks/{id}/versions")
    public List<JavaScriptFrameworkVersionDTO> getVersionsByFrameworkId(@PathVariable Long id) {
        return service.getVersions(id);
    }

    @GetMapping("/frameworks/{frameworkId}/versions/{versionId}")
    public JavaScriptFrameworkVersionDTO getVersion(@PathVariable Long frameworkId, @PathVariable Long versionId) {
        JavaScriptFrameworkVersionDTO version = service.getVersion(versionId);
        if (version.getFrameworkId() != frameworkId) {
            throw new NoSuchElementException("FrameworkId doesn't belong to Version");
        }
        return version;
    }

    @PutMapping("/frameworks/{frameworkId}/versions/{versionId}")
    public JavaScriptFrameworkVersionDTO updateVersion(@PathVariable Long frameworkId, @PathVariable Long versionId, @Valid @RequestBody JavaScriptFrameworkVersionDTO updateVersion) {
        JavaScriptFrameworkVersionDTO version = service.updateVersion(versionId, updateVersion);
        if (version.getFrameworkId() != frameworkId) {
            throw new NoSuchElementException("FrameworkId doesn't belong to Version");
        }
        return version;
    }

    @DeleteMapping("/frameworks/{frameworkId}/versions/{versionId}")
    public void updateVersion(@PathVariable Long frameworkId, @PathVariable Long versionId) {
        service.deleteVersion(frameworkId, versionId);
    }

    @PostMapping("/frameworks/{frameworkId}/versions")
    @ResponseStatus(code = HttpStatus.CREATED)
    public JavaScriptFrameworkVersionDTO saveVersion(@PathVariable Long frameworkId, @Valid @RequestBody JavaScriptFrameworkVersionDTO newVersion) {
        return service.saveVersion(newVersion, frameworkId);
    }
}



