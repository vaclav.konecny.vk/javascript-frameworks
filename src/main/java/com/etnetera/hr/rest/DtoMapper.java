package com.etnetera.hr.rest;

import com.etnetera.hr.data.entity.JavaScriptFrameworkEntity;
import com.etnetera.hr.data.entity.JavaScriptFrameworkVersionEntity;
import com.etnetera.hr.rest.dto.JavaScriptFrameworkDTO;
import com.etnetera.hr.rest.dto.JavaScriptFrameworkVersionDTO;

import java.util.stream.Collectors;

/**
 * @author Vaclav Konecny
 */
public class DtoMapper {

    public static JavaScriptFrameworkDTO toDto(JavaScriptFrameworkEntity entity) {
        JavaScriptFrameworkDTO dto = new JavaScriptFrameworkDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        if (entity.getVersions() != null) {
            dto.setVersionsId(entity.getVersions().stream().map(JavaScriptFrameworkVersionEntity::getId).collect(Collectors.toList()));
        }
        return dto;
    }

    public static JavaScriptFrameworkVersionDTO toDto(JavaScriptFrameworkVersionEntity entity) {
        JavaScriptFrameworkVersionDTO dto = new JavaScriptFrameworkVersionDTO();
        dto.setId(entity.getId());
        dto.setHype(entity.getHype());
        dto.setVersion(entity.getVersion());
        dto.setDeprecate(entity.getDeprecate());
        dto.setFrameworkId(entity.getFramework().getId());
        return dto;
    }
}
