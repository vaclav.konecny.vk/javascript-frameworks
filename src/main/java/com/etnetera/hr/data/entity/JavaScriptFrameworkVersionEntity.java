package com.etnetera.hr.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author Vaclav Konecny
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JavaScriptFrameworkVersionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Integer hype;
    private String version;
    @Column(columnDefinition = "DATE")
    private LocalDate deprecate;
    @ManyToOne(fetch = FetchType.LAZY)
    private JavaScriptFrameworkEntity framework;

    @Override
    public String toString() {
        return "FrameworkVersionEntity{" +
                "id=" + id +
                ", hype=" + hype +
                ", version='" + version + '\'' +
                ", deprecate=" + deprecate +
                ", frameworkId=" + framework.getId() +
                '}';
    }
}
