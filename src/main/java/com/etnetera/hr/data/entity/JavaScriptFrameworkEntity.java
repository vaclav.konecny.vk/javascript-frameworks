package com.etnetera.hr.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 *
 * @author Etnetera
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class JavaScriptFrameworkEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, length = 30)
	@NotBlank
	private String name;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "framework")
	private List<JavaScriptFrameworkVersionEntity> versions;

	public JavaScriptFrameworkEntity(String name) {
		this.name = name;
	}


}
