package com.etnetera.hr.data.repository;

import com.etnetera.hr.data.entity.JavaScriptFrameworkEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Spring data repository interface used for accessing the data in database.
 *
 * @author Etnetera
 */
public interface JavaScriptFrameworkRepository extends CrudRepository<JavaScriptFrameworkEntity, Long> {

    List<JavaScriptFrameworkEntity> findAllByNameContainingIgnoreCase(String name);

}
