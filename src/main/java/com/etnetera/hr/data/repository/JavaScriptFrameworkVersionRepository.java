package com.etnetera.hr.data.repository;

import com.etnetera.hr.data.entity.JavaScriptFrameworkVersionEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Vaclav Konecny
 */
public interface JavaScriptFrameworkVersionRepository extends CrudRepository<JavaScriptFrameworkVersionEntity, Long> {
    List<JavaScriptFrameworkVersionEntity> findAllByFrameworkId(long id);
}
