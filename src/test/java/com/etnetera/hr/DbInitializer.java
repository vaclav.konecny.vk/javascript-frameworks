package com.etnetera.hr;

import com.etnetera.hr.data.entity.JavaScriptFrameworkEntity;
import com.etnetera.hr.data.entity.JavaScriptFrameworkVersionEntity;
import com.etnetera.hr.data.repository.JavaScriptFrameworkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

/**
 * Insert testing data when test context is initialized.
 *
 * @author Vaclav Konecny
 */
@Component
public class DbInitializer {

    @Autowired
    private JavaScriptFrameworkRepository frameworkRepository;
    public static JavaScriptFrameworkEntity ANGULAR;

    public static JavaScriptFrameworkEntity REACT;
    public static JavaScriptFrameworkEntity VUE;

    @PostConstruct
    void init() {
        System.out.println("#######################################  Mocking up test DB - START #######################################");
        JavaScriptFrameworkEntity angular = new JavaScriptFrameworkEntity("Angular");
        angular.setVersions(List.of(new JavaScriptFrameworkVersionEntity(null, 1, "1.0", LocalDate.of(2016, Month.JULY, 14), angular),
                new JavaScriptFrameworkVersionEntity(null, 20, "2.0", LocalDate.of(2017, Month.MARCH, 23), angular),
                new JavaScriptFrameworkVersionEntity(null, 22, "4.0", LocalDate.of(2017, Month.NOVEMBER, 1), angular))
        );
        ANGULAR = frameworkRepository.save(angular);
        REACT = frameworkRepository.save(new JavaScriptFrameworkEntity("React"));
        VUE = frameworkRepository.save(new JavaScriptFrameworkEntity("Vue.js"));

        System.out.println("#######################################  Mocking up test DB - END  #######################################");
    }


}

