package com.etnetera.hr.rest;

import com.etnetera.hr.DbInitializer;
import com.etnetera.hr.data.entity.JavaScriptFrameworkEntity;
import com.etnetera.hr.data.entity.JavaScriptFrameworkVersionEntity;
import com.etnetera.hr.data.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.data.repository.JavaScriptFrameworkVersionRepository;
import com.etnetera.hr.rest.dto.JavaScriptFrameworkDTO;
import com.etnetera.hr.rest.dto.JavaScriptFrameworkVersionDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Vaclav Konecny
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class JavaScriptFrameworkControllerTest {
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    MockMvc mockMvc;
    @Autowired
    JavaScriptFrameworkRepository frameworkRepository;
    @Autowired
    JavaScriptFrameworkVersionRepository versionRepository;


    @Test
    public void frameworks_correctCall_shouldReturnAll() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/frameworks")
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void frameworkSearch_callWithANG_shouldReturnOne() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/frameworks")
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("search", "ANG")
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();

        List<JavaScriptFrameworkDTO> responseDTOs = getResponseAsListOfDTO(mvcResult, JavaScriptFrameworkDTO.class);
        assertThat(responseDTOs).hasSize(1);
        assertThat(responseDTOs.get(0).getName()).isEqualTo("Angular");
    }

    @Test
    public void frameworkSearch_callWithA_shouldReturnTwo() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/frameworks")
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("search", "A")
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();

        List<JavaScriptFrameworkDTO> responseDTOs = getResponseAsListOfDTO(mvcResult, JavaScriptFrameworkDTO.class);
        assertThat(responseDTOs).hasSize(2);
    }

    @Test
    public void frameworkById_correctCall_shouldReturnOne() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/frameworks/{id}", 1L)
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void frameworkById_unknownId_shouldReturnNoContent() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/frameworks/{id}", 10L)
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void saveFramework_correctCall_createNewFramework() throws Exception {
        JavaScriptFrameworkDTO frameworkToSave = new JavaScriptFrameworkDTO();
        frameworkToSave.setName("ember.js");
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/frameworks")
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(frameworkToSave))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated())
                .andReturn();
        JavaScriptFrameworkDTO response;
        response = getResponseAsDTO(mvcResult, JavaScriptFrameworkDTO.class);
        assertThat(response.getName()).isEqualTo(frameworkToSave.getName());
        assertThat(response.getId()).isNotZero();

    }


    @Test
    public void saveFramework_emptyName_shoudReturnBadRequest() throws Exception {
        JavaScriptFrameworkDTO frameworkToSave = new JavaScriptFrameworkDTO();
        frameworkToSave.setName("");
        mockMvc.perform(MockMvcRequestBuilders.post("/frameworks")
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(frameworkToSave))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void updateFramework_correctCall_shouldReturnUpdatedEntity() throws Exception {
        Long angularId = DbInitializer.ANGULAR.getId();
        JavaScriptFrameworkDTO frameworkToUpdate = new JavaScriptFrameworkDTO(angularId, "new Name", null);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/frameworks/{id}", angularId)
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(frameworkToUpdate))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();

        String responseContent = mvcResult.getResponse().getContentAsString();
        JavaScriptFrameworkDTO response = objectMapper.readValue(responseContent, JavaScriptFrameworkDTO.class);
        assertThat(response.getName()).isEqualTo(frameworkToUpdate.getName());
        assertThat(response.getId()).isEqualTo(angularId);

    }

    @Test
    public void updateFramework_invalidName_shouldReturnBadRequest() throws Exception {
        JavaScriptFrameworkDTO frameworkToSave = new JavaScriptFrameworkDTO();
        mockMvc.perform(MockMvcRequestBuilders.put("/frameworks/{id}", DbInitializer.ANGULAR.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(frameworkToSave))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    @Transactional
    public void deleteFramework_correctCall_shouldRemoveFrameworkAndAllVersion() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/frameworks/{id}", DbInitializer.ANGULAR.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();


        assertThat(frameworkRepository.existsById(DbInitializer.ANGULAR.getId())).isFalse();
    }

    @Test
    public void getVersionsByFrameworkId_correctCall_shouldReturnAllVersion() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/frameworks/{id}/versions", DbInitializer.ANGULAR.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();

        List<JavaScriptFrameworkVersionDTO> versionDTOS = getResponseAsListOfDTO(mvcResult, JavaScriptFrameworkVersionDTO.class);

        assertThat(versionDTOS).containsAll(DbInitializer.ANGULAR.getVersions().stream().map(DtoMapper::toDto).collect(Collectors.toList()));
    }

    @Test
    public void getVersionById_correctCall_shouldReturnCorrectVersion() throws Exception {
        JavaScriptFrameworkVersionEntity version = DbInitializer.ANGULAR.getVersions().get(0);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/frameworks/{frameworkId}/versions/{versionsId}", version.getFramework().getId(), version.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();

        JavaScriptFrameworkVersionDTO versionDTO = getResponseAsDTO(mvcResult, JavaScriptFrameworkVersionDTO.class);

        assertThat(versionDTO).isEqualTo(DtoMapper.toDto(version));
    }

    @Test
    public void getVersionsByFrameworkId_unknownFrameworkId_shouldReturnNotFound() throws Exception {
        JavaScriptFrameworkVersionEntity version = DbInitializer.ANGULAR.getVersions().get(0);
        long unknownId = 999L;
        mockMvc.perform(MockMvcRequestBuilders.get("/frameworks/{frameworkId}/versions/{versionsId}", unknownId, version.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void getVersionById_unknownVersionId_shouldReturnNotFound() throws Exception {
        JavaScriptFrameworkVersionEntity version = DbInitializer.ANGULAR.getVersions().get(0);
        long unknownId = 999L;
        mockMvc.perform(MockMvcRequestBuilders.get("/frameworks/{frameworkId}/versions/{versionsId}", version.getFramework().getId(), unknownId)
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void saveVersion_correctCall_shouldReturnSavedDTO() throws Exception {
        JavaScriptFrameworkEntity angular = DbInitializer.ANGULAR;

        JavaScriptFrameworkVersionDTO newDTO = new JavaScriptFrameworkVersionDTO();
        newDTO.setHype(10);
        newDTO.setDeprecate(LocalDate.of(2022, 12, 31));
        newDTO.setVersion("5.0");

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/frameworks/{frameworkId}/versions", angular.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newDTO))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated())
                .andReturn();
        JavaScriptFrameworkVersionDTO responseDTO = getResponseAsDTO(mvcResult, JavaScriptFrameworkVersionDTO.class);
        assertThat(angular.getVersions().size()).isLessThan(versionRepository.findAllByFrameworkId(angular.getId()).size());
        assertThat(responseDTO.getFrameworkId()).isEqualTo(angular.getId());
        assertThat(responseDTO.getVersion()).isEqualTo(newDTO.getVersion());
        assertThat(responseDTO.getDeprecate()).isEqualTo(newDTO.getDeprecate());
        assertThat(responseDTO.getHype()).isEqualTo(newDTO.getHype());
        assertThat(responseDTO.getId()).isNotZero();

    }

    @Test
    public void saveVersion_unknownFrameworkId_shouldReturnNotFound() throws Exception {
        long unknownId = 999L;
        JavaScriptFrameworkVersionDTO newDTO = new JavaScriptFrameworkVersionDTO();
        newDTO.setHype(10);
        newDTO.setDeprecate(LocalDate.of(2022, 12, 31));
        newDTO.setVersion("5.0");

        mockMvc.perform(MockMvcRequestBuilders.post("/frameworks/{frameworkId}/versions", unknownId)
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newDTO))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void saveVersion_invalidHype_shouldReturnBadRequest() throws Exception {
        JavaScriptFrameworkVersionDTO newDTO = new JavaScriptFrameworkVersionDTO();
        newDTO.setHype(-10);
        newDTO.setDeprecate(LocalDate.of(2022, 12, 31));
        newDTO.setVersion("5.0");

        mockMvc.perform(MockMvcRequestBuilders.post("/frameworks/{frameworkId}/versions", DbInitializer.ANGULAR.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newDTO))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void saveVersion_blankVersion_shouldReturnBadRequest() throws Exception {
        JavaScriptFrameworkVersionDTO newDTO = new JavaScriptFrameworkVersionDTO();
        newDTO.setHype(10);
        newDTO.setDeprecate(LocalDate.of(2022, 12, 31));
        newDTO.setVersion("");

        mockMvc.perform(MockMvcRequestBuilders.post("/frameworks/{frameworkId}/versions", DbInitializer.ANGULAR.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newDTO))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void saveVersion_nullDate_shouldReturnBadRequest() throws Exception {
        JavaScriptFrameworkVersionDTO newDTO = new JavaScriptFrameworkVersionDTO();
        newDTO.setHype(10);
        newDTO.setDeprecate(null);
        newDTO.setVersion("5.0");

        mockMvc.perform(MockMvcRequestBuilders.post("/frameworks/{frameworkId}/versions", DbInitializer.ANGULAR.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newDTO))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void updateVersion_correctCall_shouldReturnUpdatedDTO() throws Exception {
        JavaScriptFrameworkEntity angular = DbInitializer.ANGULAR;
        JavaScriptFrameworkVersionEntity versionEntity = angular.getVersions().get(0);
        JavaScriptFrameworkVersionDTO frameworkVersionDTO = DtoMapper.toDto(versionEntity).withHype(100);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/frameworks/{frameworkId}/versions/{versionId}", angular.getId(), frameworkVersionDTO.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(frameworkVersionDTO))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();

        JavaScriptFrameworkVersionDTO responseDTO = getResponseAsDTO(mvcResult, JavaScriptFrameworkVersionDTO.class);
        assertThat(responseDTO.getId()).isEqualTo(frameworkVersionDTO.getId());
        assertThat(responseDTO.getHype()).isEqualTo(frameworkVersionDTO.getHype());

    }

    @Test
    public void deleteVersion_correctCall_shouldDeleteVersion() throws Exception {
        JavaScriptFrameworkEntity angular = DbInitializer.ANGULAR;
        JavaScriptFrameworkVersionEntity versionEntity = angular.getVersions().get(0);

        mockMvc.perform(MockMvcRequestBuilders.delete("/frameworks/{frameworkId}/versions/{versionId}", angular.getId(), versionEntity.getId())
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();

        assertThat(versionRepository.findById(versionEntity.getId())).isEmpty();

    }


    //******************************* utility methods ********************************

    private <T> T getResponseAsDTO(MvcResult mvcResult, Class<T> dtoClass) throws IOException {
        String responseContent = mvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(responseContent, dtoClass);
    }

    private <T> List<T> getResponseAsListOfDTO(MvcResult mvcResult, Class<T> dtoClass) throws IOException {
        String responseContent = mvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(responseContent, objectMapper.getTypeFactory().constructCollectionType(List.class, dtoClass));
    }

    public String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}